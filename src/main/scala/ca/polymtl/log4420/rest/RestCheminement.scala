package ca.polymtl.log4420
package rest

import model.Cheminement

import net.liftweb._
import http.rest.RestHelper
import common.Box
import json.JsonAST.JValue

// Rogue
import com.foursquare.rogue.Rogue._

// Bson
import org.bson.types.ObjectId


object RestCheminement extends RestHelper
{
  serve( "cheminement" :: Nil prefix {

    /*
     * Affiche tous les cheminements
     */
    case Nil JsonGet _ => Cheminement.findAll: JValue

    /*
    * Affiche un seul cheminement
    */
    case id :: Nil JsonGet _ => Cheminement where( _._id eqs new ObjectId( id ) ) get() : Option[JValue]

    /*
     * Ajoute un cheminement
     */
    case Nil JsonPost ( json -> _ ) => updateFromJson( json ) : Box[JValue]

    /*
     * Modifie un cheminement
     */
    case id :: Nil JsonPut ( json -> _ ) => updateFromJson( json ) : Box[JValue]

    /*
     * Suprime un cheminement
     */
    case id :: Nil JsonDelete _ => Cheminement where( _._id eqs new ObjectId( id ) ) findAndDeleteOne() : Option[JValue]

  })

    private def updateFromJson( json: JValue ) =
    {
      val cheminement = Cheminement.createRecord
      cheminement.setFieldsFromJValue( json ).map( _ => cheminement.save )
    }
}
