#! /bin/sh

# Get all
curl  localhost:8080/cheminement

# Get one
# curl

# curl

# curl -X DELETE

# curl -X PUT -H "Content-type: application/json" localhost:8080 -d ''

curl -X POST -H "Content-type: application/json" localhost:8080/cheminement -d '{
    "sessions":
    [
        "class ca.polymtl.log4420.model.Session=
        {
            listeDeCours=List(Cours(INF1005C), Cours(INF1500), Cours(MTH1101), Cours(MTH1006), Cours(LOG3005I), Cours(INF1040)),
            perriode=Perriode(Automne,2009)
        }",
        "class ca.polymtl.log4420.model.Session={
            listeDeCours=List(Cours(INF1010), Cours(LOG1000), Cours(INF1600), Cours(MTH1102), Cours(INF1995)),
            perriode=Perriode(Hiver,2010)
        }"
    ],
    "titre":"Multimedia",
    "owner":"Génie Logiciel"
}'