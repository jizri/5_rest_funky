package ca.polymtl.log4420.model

import net.liftweb._
import mongodb.record._
import field.{MongoCaseClassField, MongoListField}
import ca.polymtl.log4420.lib.RestMongo

class Session private()
  extends BsonRecord[Session]
{
  def meta = Session

  object perriode extends MongoCaseClassField[Session, Perriode]( this )
  object listeDeCours extends MongoListField[Session, Cours]( this )
}

object Session extends Session with BsonMetaRecord[Session]

case class Cours( sigle: String )
case class Perriode( saison: Saison, annee: Int )

sealed trait Saison
case object Automne extends Saison
case object Hiver extends Saison
case object Ete extends Saison