package ca.polymtl.log4420
package rest

import model.Cheminement

import net.liftweb._
import http.rest.RestHelper
import json.JsonAST.JValue

// Rogue
import com.foursquare.rogue.Rogue._

// Bson
import org.bson.types.ObjectId


object RestSession extends RestHelper
{
  serve( "cheminement" :: Nil prefix {

        /*
         * Affiche la ième session
         */
        case id :: "session" :: i :: Nil  JsonGet _ =>  Cheminement where( _._id eqs new ObjectId( id ) ) get() : Option[JValue]

        /*
         * Affiche le jième cours de la ième session
         */
        case id :: "session" :: i :: "cours" :: j :: Nil  JsonGet _ => ???

        /*
         * Ajoute une session à la fin
         */
        case id :: "session" :: Nil JsonPost _ => ???

        /*
         * Suprime la ième session
         */
        case id :: "session" :: i :: Nil  JsonDelete _ => ???

        /*
         * Deplacer cours
         */
        //case id :: Nil JsonPut _ => ???
  })
}
