package ca.polymtl.log4420
package model

import lib.RestMongo

import net.liftweb._
import common.Box
import json.JsonAST.{JArray, JValue}

import mongodb.record._
import field.MongoListField
import record.field.StringField

import com.foursquare.rogue.Rogue._

import org.bson.types.ObjectId

class Cheminement private()
  extends MongoRecord[Cheminement]
  with MongoId[Cheminement]
{
  def meta = Cheminement

  object titre extends StringField(this, 1023)
  object owner extends StringField(this, 1023)
  object sessions extends MongoListField[Cheminement, Session](this)

  implicit def toJson(cheminement : Cheminement): JValue = cheminement.asJValue
  implicit def toJsonFromSeq(cheminement : Seq[Cheminement]): JValue = JArray( cheminement.map( c => (c.asJValue): JValue ).toList )
  implicit def toJsonFromOption( cheminement: Option[Cheminement] ): Option[JValue] = cheminement.map( _.asJValue )
  implicit def toJsonFromBox( cheminement: Box[Cheminement] ): Box[JValue] = cheminement.map(_.asJValue )
}

object Cheminement extends Cheminement with MongoMetaRecord[Cheminement]
{
  override def mongoIdentifier = RestMongo
}