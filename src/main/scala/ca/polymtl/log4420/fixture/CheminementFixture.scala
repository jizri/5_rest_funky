package ca.polymtl.log4420
package fixture

import model._

object CheminementFixture
{
  def insert()
  {
    if ( Cheminement.count == 0 )
    {
      Cheminement.createRecord
        .titre("Classique")
        .owner("Génie Logiciel")
        .sessions(List(
        Session.createRecord
          .perriode( Perriode( Automne, 2009 ))
          .listeDeCours( List(
          Cours( "INF1005C"),
          Cours( "INF1500" ),
          Cours( "MTH1101" ),
          Cours( "MTH1006"),
          Cours( "LOG3005I"),
          Cours( "INF1040")
        )),
        Session.createRecord
          .perriode ( Perriode( Hiver, 2010 ) )
          .listeDeCours( List(
          Cours( "INF1010" ),
          Cours( "LOG1000" ),
          Cours( "INF1600" ),
          Cours( "MTH1102" ),
          Cours( "INF1995")
        ))
        // [...]
      )).save
    }
  }
}