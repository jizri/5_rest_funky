package bootstrap.liftweb

import net.liftweb._
import http.LiftRules

import ca.polymtl.log4420._
import lib.RestMongo
import fixture.CheminementFixture
import rest.RestCheminement

class Boot 
{
	def boot()
	{
    RestMongo.start()
    CheminementFixture.insert()

    LiftRules.dispatch.append( RestCheminement )
		LiftRules.early.append( _.setCharacterEncoding("UTF-8") )
	}
}